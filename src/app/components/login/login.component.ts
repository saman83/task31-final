import { AuthService } from './../../services/auth/auth.service';
import { Router } from '@angular/router';
import { SessionService } from './../../services/session/session.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {
    username: '',
    password: ''
  }

  isLoading: boolean = false;
  loginError: string;


  constructor(private session: SessionService, private auth: AuthService, private router: Router) {
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard')
    }
   }

  ngOnInit(): void {
  }

  async onLoginClicked() {

    this.loginError = '';


    try {
      this.isLoading = true;
      const result: any = await this.auth.login( this.user );
      console.log(result);
      
      if (result.status < 400 ) {
        this.session.save( { token: result.data.token, username: result.data.user.username });
        this.router.navigateByUrl('/dashboard');

      }


    } catch (e) {
      this.loginError = e.error.error;
      
    } finally {
      this.isLoading = false;
    }
  }
}
